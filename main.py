from address import Address
from mailing import Mailing

mail = Mailing(
    Address(12345, 'Chita', 'Bagulnik', 18, 12),
    Address(123453, 'Moscow', 'Lesnaya', 123, 125),
    123,
    1234567,
)

print(
    'Отправление ' +  str(mail.from_address.index) + ' из ' + mail.from_address.city + ' ' + mail.from_address.street + ' ' +  str(mail.from_address.build) + ' - ' +  str(mail.from_address.flat) +
    ' В: ' +  str(mail.to_address.index) + mail.to_address.city + ' ' + mail.to_address.street + ' ' +  str(mail.to_address.build) + ' - ' +  str(mail.to_address.flat) +
    ' Стоимость: ' + str(mail.cost) + 'рублей')
